# ONES is a niubility ERP system

作者：闫志鹏 <335454250@qq.com>

## 关于ONES 

ONES not ONS，项目前端基于AngularJS+Bootstrap, 后端使用PHP+MySQL，使用ThinkPHP框架。目标是打造可解决实际问题优秀的B/S开源ERP系统。目前仍在开发过程中。

## 特色

* 致力于解决实际问题
* APP化的项目组成 + 灵活的前后端插件机制
* 独特的CommonView功能，无需重复编写的前端文件
* 灵活的数据模型功能，可方便实现数据字段扩展
* 灵活可定制的工作流程，工作流程自带防止并发操作机制

## 文档
* [ONES使用手册](http://project.ng-erp.com/projects/ones/wiki/ONES_ERP_%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C)
* [ONES APP使用手册](http://project.ng-erp.com/projects/ones/wiki/APP%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C%E7%B4%A2%E5%BC%95)
* [ONES 开发文档](http://project.ng-erp.com/projects/ones/wiki/ONES_ERP_%E5%BC%80%E5%8F%91%E6%96%87%E6%A1%A3)


## 链接

* 演示见：http://demo.ng-erp.com
* 讨论/反馈：http://project.ng-erp.com/projects/ones